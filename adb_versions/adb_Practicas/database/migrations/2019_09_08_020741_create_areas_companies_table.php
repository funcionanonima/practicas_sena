<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas_companies', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('area_id');
            $table->unsignedBigInteger('companie_id');
            $table->unique(['area_id', 'companie_id']);
            $table->string('body');
            $table->string('image');

            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies_areas');
    }
}
