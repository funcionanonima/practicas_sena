<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('role_id')->default(1);
            $table->unsignedBigInteger('id_type_id')->default(2);

            $table->string('name', 190);
            $table->string('identification', 40)->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->enum('status',['ACTIVO', 'INACTIVO']);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
