<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create([
            'name' => 'usuario',
            'body' => 'Usuario común que registra sus empresas'
            ]);
        $role = Role::create([
            'name' => 'administrador',
            'body' => 'Usuario con acceso al manejo de informacion del sitio'
            ]);
    }
}
