<?php

use App\Document;
use Illuminate\Database\Seeder;

class DocumentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Document::create([
            'area_companie_id' => 1,
            'name' => 'Documento Word',
            'path' => '1569391748TALLER 1 DE BASES DE DATOS AVANZADOS(1).docx',
            'status' => 'ACTIVO'
        ]);
        Document::create([
            'area_companie_id' => 2,
            'name' => 'Documento Word',
            'path' => '1569391748TALLER 1 DE BASES DE DATOS AVANZADOS(1).docx',
            'status' => 'ACTIVO'
        ]);
        Document::create([
            'area_companie_id' => 3,
            'name' => 'DocumentoPDF',
            'path' => '1571095145RespuestaSolicitud.pdf',
            'status' => 'ACTIVO'
        ]);
        Document::create([
            'area_companie_id' => 3,
            'name' => 'Documento Word',
            'path' => '1569391748TALLER 1 DE BASES DE DATOS AVANZADOS(1).docx',
            'status' => 'ACTIVO'
        ]);
        Document::create([
            'area_companie_id' => 3,
            'name' => 'DocumentoPDF',
            'path' => '1571095145RespuestaSolicitud.pdf',
            'status' => 'ACTIVO'
        ]);
        Document::create([
            'area_companie_id' => 3,
            'name' => 'DocumentoPDF',
            'path' => '1571095145RespuestaSolicitud.pdf',
            'status' => 'ACTIVO'
        ]);
    }
}
