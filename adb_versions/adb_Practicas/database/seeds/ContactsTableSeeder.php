<?php

use App\Contact;
use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contact = new Contact();
        $contact->name = 'Ramiro Arrieta';
        $contact->companie = 'AdcuadProyetos';
        $contact->email = 'email@email.com';
        $contact->phone = '98765432';
        $contact->body = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex, quisquam.';
        $contact->TC = true;

        $contact->save();
    }
}
