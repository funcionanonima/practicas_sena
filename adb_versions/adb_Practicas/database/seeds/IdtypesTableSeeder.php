<?php

use Illuminate\Database\Seeder;
use App\IdType;

class IdTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    $idType = IdType::create([
        'abbreviation' => 'NIT',
        'name' => 'Número de Identificación Tributaria',
        'body' => 'es un número único colombiano que asigna la DIAN (Dirección de Impuestos y Aduanas Nacionales de Colombia) por una sola vez cuando el obligado se inscribe en el RUT (Registro Único Tributario).',
        ]);
    $idType = IdType::create([
        'abbreviation' => 'CC',
        'name' => 'Cédula de Ciudadanía',
        'body' => 'Documento emitido por una autoridad administrativa competente para permitir la identificación personal de los ciudadanos.'
        ]);
    $idType = IdType::create([
        'abbreviation' => 'TI',
        'name' => 'Tarjeta de Identidad',
        'body' => 'Documento se tramita a los menores de edad colombianos que hayan cumplido siete años. ... Una vez se cumpla la mayoría de edad (18 años), la tarjeta de identidad pierde su validez.'
        ]);
    }
}
