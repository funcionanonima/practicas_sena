<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'administrador';
        $user->email = 'admin@admin.com';
        $user->password = bcrypt('admin');
        $user->role_id = 2;
        $user->id_type_id = 2;
        $user->status = 'ACTIVO';
        $user->identification = 'E22MNgdrr445';
        $user->save();
        
        $user = new User();
        $user->name = 'usuario';
        $user->email = 'user@user.com';
        $user->password = bcrypt('user');
        $user->role_id = 1;
        $user->id_type_id = 2;
        $user->status = 'ACTIVO';
        $user->identification = 'EMNRT445';
        $user->save();

        $user = new User();
        $user->name = 'usuario2';
        $user->email = 'user2@user.com';
        $user->password = bcrypt('user2');
        $user->role_id = 1;
        $user->id_type_id = 2;
        $user->status = 'INACTIVO';
        $user->identification = 'EMNRTRRES22445';
        $user->save();
    }
}
