<?php

use App\Area;
use Illuminate\Database\Seeder;

class AreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $area = Area::create([
            'name' => 'comunicaciones',
            'body' => 'Recibe quejas de lo usuarios'
        ]);
        $area = Area::create([
            'name' => 'soporte',
            'body' => 'Apoya los procesos de todas las àreas',
        ]);
        $area = Area::create([
            'name' => 'logistica',
            'body' => 'coordinan procesos de las démas áreas',
        ]);
        $area = Area::create([
            'name' => 'desarrollo',
            'body' => 'codifican el producto',
        ]);
    }
}
