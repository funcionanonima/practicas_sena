<?php

use App\Companie;
use App\User;
use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $comp = new Companie();
        $comp->identification = '987755NNo';
        $comp->id_type_id =  1;
        $comp->name = 'Noel';
        $comp->body = 'Empresa de galletas';
        $comp->status = 'ACTIVO';
        $comp->save();
        $comp->users()->attach(User::findOrFail(2));

        $comp = new Companie();
        $comp->identification = 'VB4563GT';
        $comp->id_type_id = 1;
        $comp->name = 'Argos';
        $comp->body = 'Empresa de cementos, secor de la construcciòn';
        $comp->status = 'ACTIVO';
        $comp->save();
        $comp->users()->attach(User::findOrFail(3));

        $comp = new Companie();
        $comp->identification = 'AAWED4577';
        $comp->id_type_id = 1;
        $comp->name = 'Vaniplast';
        $comp->body = 'Empresa de plasticos';
        $comp->status = 'ACTIVO';
        $comp->save();
        $comp->users()->attach(User::findOrFail(2));

        $comp = new Companie();
        $comp->identification = 'VCRR566';
        $comp->id_type_id = 2;
        $comp->name = 'Enka';
        $comp->body = 'Empresa Textil';
        $comp->status = 'INACTIVO';
        $comp->save();
        $comp->users()->attach(User::findOrFail(2));
    }
}
