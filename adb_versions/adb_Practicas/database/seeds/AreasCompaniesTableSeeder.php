<?php

use App\AreaCompanie;
use Illuminate\Database\Seeder;

class AreasCompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $det = AreaCompanie::create([
            'area_id' => 1,
            'companie_id' => 1,
            'body' => 'Reporte Anual de ingresos',
            'image' => '123.jpeg',
        ]);
        $det = AreaCompanie::create([
            'area_id' => 2,
            'companie_id' => 1,
            'body' => 'Reporte anual de egresos',
            'image' => '1234.jpg',
        ]);
        $det = AreaCompanie::create([
            'area_id' => 3,
            'companie_id' => 1,
            'body' => 'Reprte actividades anuales',
            'image' => '45.png',
        ]);
        // $det = AreaCompanie::create([
        //     'area_id' => 2,
        //     'companie_id' => 2,
        //     'body' => 'body1',
        //     'image' => 'image1',
        // ]);
        // $det = AreaCompanie::create([
        //     'area_id' => 1,
        //     'companie_id' => 1,
        //     'body' => 'body1',
        //     'image' => 'image1',
        // ]);
        // $det = AreaCompanie::create([
        //     'area_id' => 2,
        //     'companie_id' => 2,
        //     'body' => 'body1',
        //     'image' => 'image1',
        // ]);
        // $det = AreaCompanie::create([
        //     'area_id' => 2,
        //     'companie_id' => 2,
        //     'body' => 'body1',
        //     'image' => 'image1',
        // ]);
        $det = AreaCompanie::create([
            'area_id' => 2,
            'companie_id' => 4,
            'body' => 'reporte de reportes',
            'image' => '67.png',
        ]);
    }
}


