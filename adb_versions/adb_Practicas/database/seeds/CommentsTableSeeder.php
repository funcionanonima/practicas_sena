<?php

use Illuminate\Database\Seeder;

use App\Comment;
class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Comment::create([
            'area_companie_id' => 1,
            'body' => 'estado malo',
        ]);
        Comment::create([
            'area_companie_id' => 2,
            'body' => 'estado malo',
        ]);
        Comment::create([
            'area_companie_id' => 3,
            'body' => 'estado malo',
        ]);
    }
}
