<?php

// Se tiene 3 middlewares
// Administrador
// Usuario 
// Administrador&Usuario
// Según la necesidad de la ruta se le asigna middleware al método

Auth::routes();

Route::get('/', 'HomeController@wellcome')->name('wellcome');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/goal', 'HomeController@goal')->name('goal');
Route::get('/mailus', 'HomeController@mailus')->name('mailus');

Route::resource('/report', 'AreaCompanieController')->middleware('auth');
Route::resource('/idtype', 'IdTypeController')->middleware('admin');
Route::resource('/role', 'RoleController')->middleware('admin');
Route::resource('/user', 'UserController')->middleware('admin');
Route::resource('/companie', 'CompanieController')->middleware('admin');
Route::resource('/area', 'AreaController')->middleware('admin');
Route::resource('/document', 'DocumentController')->middleware('admin');
Route::resource('/comment', 'CommentController')->middleware('adminuser');
Route::resource('/contact', 'ContactController')->middleware('admin');