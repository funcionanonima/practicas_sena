@extends('layouts.app')
    @section('content')
        <div class="">

            <div class="d-block mx-auto my-2" style="width: 80%;">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="{{ asset('images/nn.png') }}" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Primer carrousell</h5>
                                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Provident, voluptas.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('images/4.jpg') }}" alt="Second slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Segundo carrousell</h5>
                                <p>Lorem ipsum dolor sit.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('images/3.jpg') }}" alt="Third slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Tercer carrousell</h5>
                                <p>Lorem ipsum dolor sit amet.</p>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>

            {{-- <div class="text-center">
                <img class="img-fluid" src="{{ asset('images/nn.png') }}" alt="no">
            </div> --}}

            <div class="p-5 text-center bg-white">
                <div class="container">
                    <h2 class="text-dark mb-4" >¿Quienes somos?</h2> 
                    <hr class=""> 
                    <p class="mt-4 text-dark text-justify h5">Somos una empresa conformada por un grupo de profesionales que asesoramos y facilitamos la administración de procesos apoyados en información de los diferentes sectores económicos y basados en análisis estadísticos para la toma de decisiones. Nos apasiona analizar y evaluar el impacto de las políticas públicas en el sector privado y generar alternativas para su aprovechamiento.</p>      
                </div>              
            </div>
            
            <div class="text-center m-5" >
                <img  width="180" src="{{ asset('images/adb1.png') }}" class="m-4" alt="">
                <br>
                <span class="text-warning">
                    +57 3163413215
                </span>    
                <br>        
                <span class="text-warning">
                    wilopez@adbconsulting.com.co
                </span>
            </div>      
                        
            {{-- goupbutton --}}
        <button onclick="topFunction()" id="topBtn" title="Up" class="bg-warning text-white"><i class="fa fa-arrow-circle-up" aria-hidden="true"></i></button>
        </div>        
    @endsection
