<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>    

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/btn.css') }}" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
    <body>
        {{-- <div id="app"> --}}        
        {{-- <div class="d-flex" id="wrapper"> --}}
                
            <!-- Page Content -->
            {{-- <div id="page-content-wrapper"> --}}

                <nav class="navbar navbar-expand-sm navbar-light bg-white shadow-sm" id="navbar">
                    <div class="container">
                        <a class="navbar-brand" href="{{ url('/') }}">
                            {{-- {{ config('app.name', 'Laravel') }} --}}
                            <img width="200" class="" src="{{ asset('images/adb1.png') }}" alt="">
                        </a>
                        
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                
                        <div class="collapse navbar-collapse  pt-2" id="navbarSupportedContent">
                            <!-- Left Side Of Navbar -->

                            <!-- Right Side Of Navbar -->
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">                                    
                                    <a class="nav-link" href="{{route('wellcome')}}"><i class="fa fa-home" aria-hidden="true"></i>
                                        Inicio</a></a>
                                </li>
                                <li class="nav-item">                                    
                                    <a class="nav-link" href="{{route('goal')}}"><i class="fa fa-compass" aria-hidden="true"></i> Visión</a></a>
                                </li>
                                <li class="nav-item">                                    
                                    <a class="nav-link" href="{{route('mailus')}}"><i class="fa fa-envelope-o" aria-hidden="true"></i> Contáctenos</a></a>
                                </li>  
                                <!-- Authentication Links -->
                                @guest                                    
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('login') }}"><i class="fa fa-user-o" aria-hidden="true"></i> {{ __(' Iniciar sesion') }}</a>
                                    </li>
                                @else  
                                    <li class="nav-item dropdown">
                                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>
                
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="{{ route('home') }}"><i class="fa fa-check-square-o" aria-hidden="true"></i> Reportes</a></a>
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                                                <i class="fa fa-power-off" aria-hidden="true"></i>
                                                {{ __('Cerrar Sesion') }}                                                
                                            </a>
                
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </div>
                                    </li>
                                @endguest
                            </ul>
                        </div>
                    </div>
                </nav>            
                
                <div class="">
                    @yield('content')
                </div>

                <footer class="bg-dark footer pt-4">
                    <div class="container">
                        <div class="row">
                            <div class="col-6">
                                <ul class="text-light p-0" style="list-style:none">
                                    <li>{{ config('app.name') }}</li>
                                    <li>+57 3163413215</li>
                                    <li>wilopez@adbconsulting.com.co</li>
                                </ul>
                            </div>
                            <div class="col-6">
                                <p class="text-light text-right">Copyright © - ADB Consulting SAS - Derechos reservados</p>
                                <p class="text-light text-right">Medellìn - Colombia</p>
                            </div>
                        </div>
                    </div>
                </footer>
            {{-- </div> --}}
            <!-- /#page-content-wrapper -->
        {{-- </div>     --}}
        {{-- </div> --}}
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="{{ asset('js/btn.js') }}" defer></script>
    </body>
</html>
