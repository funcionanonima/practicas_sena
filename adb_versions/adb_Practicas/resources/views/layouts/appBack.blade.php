<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>
    {{-- <div id="app"> --}}        
    <div class="d-flex" id="wrapper">       

        <!-- Sidebar -->
        <div class="bg-light border-right" id="sidebar-wrapper">
            <div class="sidebar-heading text-center">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img width="120" class="" src="{{ asset('images/adb1.png') }}" alt="">
                </a>
            </div>
            @user            
            <a href="{{ url('/home') }}" class="mb-2 list-group-item list-group-item-action bg-light"><i class="fa fa-home" aria-hidden="true"></i> Principal</a>
            <div class="list-group list-group-flush">
                @isset($companies)
                    @if(count($companies)>0)
                        @foreach($companies as $key=>$companie)  

                        {{-- collapsebutton --}}
                        <a href="#" class="list-group-item list-group-item-action bg-light navbar-toggler" data-toggle="collapse" data-target="#lendo{{$key}}">{{$companie->name}}<i style="float: right; display:inline;" class="fa fa-caret-down" aria-hidden="true"></i></a>
                        {{-- collapse --}}
                        <div class="collapse navbar-collapse" id="lendo{{$key}}">
                            @if(count($companie->areas)>0)                            
                                @foreach($areas = $companie->areas()->distinct()->get() as $area) 
                                <a href="{{route('report.show', $report = $companie->areascompanies->where('area_id', $area->id)->last())}}" class="pl-5 list-group-item list-group-item-action">{{$area->name}} </a>
                                @endforeach 
                            @else
                            <a href="#" class="pl-5 list-group-item list-group-item-action">No tiene áreas asociadas</a>
                            @endif
                        </div>
                        {{-- endcollapse --}}
                        @endforeach 
                    @else
                    <a href="#" class="pl-5 list-group-item list-group-item-action">No Registra Empresas</a>
                    @endif
                @endisset

                {{-- collapse --}}
                <div class="collapse navbar-collapse" id="lendo">
                    
                    <a href="{{ route('companie.index') }}" class="pl-5 list-group-item list-group-item-action bg-light"><i class="fa fa-building-o" aria-hidden="true"></i> Empresas</a>
                </div>
                {{-- endcollapse --}}
                
                
                {{-- <button class="m-2 btn btn-dark btn-sm menu-togglet" id="menu-togglet">Menú</button> --}}
                

                <a class="mt-2 list-group-item list-group-item-action bg-light" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    <i class="fa fa-power-off" aria-hidden="true"></i>
                    {{ __('Cerrar Sesión') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>      
            </div>
            @enduser
            @admin
            <div class="list-group list-group-flush">
                <a href="{{ url('/home') }}" class="mb-2 list-group-item list-group-item-action bg-light"><i class="fa fa-home" aria-hidden="true"></i> Principal</a>
                
                {{-- collapsebutton --}}
                <a href="#" class="list-group-item list-group-item-action bg-light navbar-toggler" data-toggle="collapse" data-target="#manage"><i class="fa fa-handshake-o" aria-hidden="true"></i> Gestion<i style="float: right; display:inline;" class="fa fa-caret-down" aria-hidden="true"></i></a>
                {{-- collapse --}}
                <div class="collapse navbar-collapse" id="manage">
                <a href="{{ route('user.index') }}" class="pl-5 list-group-item list-group-item-action bg-light"><i class="fa fa-users" aria-hidden="true"></i> Usuarios</a>                
                <a href="{{ route('companie.index') }}" class="pl-5 list-group-item list-group-item-action bg-light"><i class="fa fa-building-o" aria-hidden="true"></i> Empresas</a>
                </div>
                {{-- endcollapse --}}
            
                {{-- collapsebutton --}}
                <a href="#" class="list-group-item list-group-item-action bg-light navbar-toggler" data-toggle="collapse" data-target="#resources"><i class="fa fa-paperclip" aria-hidden="true"></i> Recursos<i style="float: right; display:inline;" class="fa fa-caret-down" aria-hidden="true"></i></a>
                {{-- collapse --}}
                <div class="collapse navbar-collapse" id="resources">
                <a href="{{route('document.index')}}" class="pl-5 list-group-item list-group-item-action bg-light"><i class="fa fa-folder-open-o" aria-hidden="true"></i> Documentos</a>
                <a href="{{route('comment.index')}}" class="pl-5 list-group-item list-group-item-action bg-light"><i class="fa fa-commenting-o" aria-hidden="true"></i> Comentarios</a>
                <a href="{{route('contact.index')}}" class="pl-5 list-group-item list-group-item-action bg-light"><i class="fa fa-phone" aria-hidden="true"></i> Contactos</a>

                </div>
                {{-- endcollapse --}}

                {{-- collapsebutton --}}
                <a href="#" class="list-group-item list-group-item-action bg-light navbar-toggler" data-toggle="collapse" data-target="#lendo"><i class="fa fa-cog" aria-hidden="true"></i> Parámetros<i style="float: right; display:inline;" class="fa fa-caret-down" aria-hidden="true"></i></a>
                {{-- collapse --}}
                <div class="collapse navbar-collapse" id="lendo">
                <a href="{{ route('idtype.index') }}" class="pl-5 list-group-item list-group-item-action bg-light"><i class="fa fa-address-card-o" aria-hidden="true"></i> Tipos de Identificacion</a>         
                <a href="{{ route('area.index') }}" class="pl-5 list-group-item list-group-item-action bg-light"><i class="fa fa-thumb-tack" aria-hidden="true"></i> Areas</a>
                <a href="{{ route('role.index') }}" class="pl-5 list-group-item list-group-item-action bg-light"><i class="fa fa-key" aria-hidden="true"></i> Roles</a>


                </div>
                {{-- endcollapse --}}
                <a href="{{route('report.index')}}" class="bg-dark text-light mt-2 list-group-item list-group-item-action"><i class="fa fa-file-text-o" aria-hidden="true"></i> Reportes</a>

                
                {{-- <hr> --}}
                <a class="mt-2 list-group-item list-group-item-action bg-light" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    <i class="fa fa-power-off" aria-hidden="true"></i>
                    {{ __('Cerrar Sesion') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                       
            </div>
            @endadmin
        </div>
        <!-- /#sidebar-wrapper -->
            
        <!-- Page Content -->
        <div id="page-content-wrapper">

            <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
                    <div class="container">   
                        <ul class="navbar-nav mr-auto">
                                {{-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#lendo" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                                    <span class="navbar-toggler-icon"></span>
                                </button> --}}
                            <button class="btn btn-dark content-left btn-sm menu-togglet" id="menu-toggle"><i class="fa fa-exchange" aria-hidden="true"></i></button>
                        </ul>
                        
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                
                        <div class="collapse navbar-collapse pt-2" id="navbarSupportedContent">
                            <!-- Left Side Of Navbar -->
                            <ul class="navbar-nav mr-auto">
                                @auth
                                <li class="nav-item">
                                    
                                    
                                </li>
                                @endauth
                            </ul>
                            <!-- Right Side Of Navbar -->
                            <ul class="navbar-nav ml-auto">
                                <!-- Authentication Links -->
                                @guest
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('login') }}"><i class="fa fa-user-o" aria-hidden="true"></i>{{ __(' Iniciar Sesion') }}</a>
                                    </li>
                                @else
                                    <li class="nav-item dropdown">
                                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>
                
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="{{ route('home') }}"><i class="fa fa-check-square-o" aria-hidden="true"></i> Reportes</a>
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                                                <i class="fa fa-power-off" aria-hidden="true"></i>
                                                {{ __('Cerrar Sesion') }}
                                            </a>
                
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </div>
                                    </li>
                                @endguest
                            </ul>
                        </div>
                    </div>
                </nav>            
            
            <div class="container-fluid">
                @yield('content')
            </div>                       
        </div>
        <!-- /#page-content-wrapper -->        
    </div>    
    <footer class="bg-dark footer pt-2 mt-4">
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <ul class="text-light p-0" style="list-style:none">
                        <li>{{ config('app.name') }}</li>
                        <li>+57 3163413215</li>
                        <li>wilopez@adbconsulting.com.co</li>
                    </ul>
                </div>
                <div class="col-6">
                    <p class="text-light text-right">Copyright © - ADB Consulting SAS - Derechos reservados</p>
                </div>
            </div>
        </div>
    </footer>
    
    {{-- </div> --}}
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous">
    </script>
    <script>
        $(".menu-togglet").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        });
    </script>
</body>
</html>
