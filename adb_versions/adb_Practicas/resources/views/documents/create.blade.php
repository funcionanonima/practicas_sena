@extends('layouts.appBack')

@section('content')
    {{-- bread --}}
    <nav aria-label="breadcrumb" class="my-4">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{route('document.index')}}">Documentos</a></li>
            <li class="breadcrumb-item active" aria-current="page">Nuevo Documento</li>
        </ol>
    </nav>
    {{-- endbread --}}
    <h1 class="my-5 text-center">Registrar Documento</h1> 
    
    <div class="card m-2 p-5">

        <div class="mb-5">
            <a href="{{route('document.index')}}"><i class="fa fa-hand-o-left" aria-hidden="true"></i> Regresar</a>
        </div>

        <form method="POST" action="{{ route('document.store') }}" enctype="multipart/form-data">
            @csrf

            <div class="row">
                <div class="form-group col">
                    <input type="text" id="name" class="form-control @error('name') is-invalid @enderror" name="name" required autocomplete="new-body" placeholder="Nombre">
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>                        
            </div> 

            <div class="row">

                <div class="form-group col-12">                            
                    <select id="area_companie_id" class="form-control @error('area_companie_id') is-invalid @enderror" name="area_companie_id" required >
                            <option value="" selected disabled>Reporte</option>
                        @foreach($reports as $report)
                            <option value="{{$report->id}}">{{$report->body}}</option>
                        @endforeach
                    </select>
                    @error('area_companie_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>                        
                
            </div>     

            <div class="row border m-1">
                <div class="form-group col-12 d-inline">
                    <label for="path">Archivo</label> 
                    <input type="file" name="path" class="form-control-file" id="path">
                </div>
                @error('path')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>   

            <div class="row mt-5">
                <div class="form-group col text-center">
                    <button type="submit" class="btn btn-dark">
                        {{ __('Registrar') }}
                    </button>
                </div>                
            </div>

        </form>

    </div>
@endsection
