@extends('layouts.appBack')
    @section('content')
        {{-- bread --}}
        <nav aria-label="breadcrumb" class="my-4">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Documentos</li>
            </ol>
        </nav>
        {{-- endbread --}}
        <h1 class="my-5 text-center">Control Documentos</h1>          
         
        <div class="card p-4">

            <div class="mb-4 container-fluid">
                <a class="btn btn-dark" href="{{route('document.create')}}">Nuevo</a>
            </div>
            
            <div class="container-fluid mx-auto">
                <table class="table table-hover table-striped table-responsive-xl border">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Reporte</th>
                            <th>Empresa</th>
                            <th>Área</th>
                            <th>Fecha Creación</th>    
                            <th width="5%"class="text-center">Editar</th> 
                            <th width="5%"class="text-center">Estado</th>                                
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($documents as $key=>$document)
                        <tr>
                            <td><a href="documents/{{$document->path}}">{{$document->name}}</a></td>
                            <td>{{$document->report->body}}</td>
                            <td>{{$document->report->companie->name}}</td>
                            <td>{{$document->report->area->name}}</td>
                            <td>{{$document->created_at->day}}/{{$document->created_at->month}}/{{$document->created_at->year}}</td>
                            <td class="text-center">
                                <a href="{{route('document.edit', $document)}}" class="btn btn-sm"><i class="fa fa-pencil text-warning" aria-hidden="true"></i></a>
                            </td>
                            <td class="text-center">
                                <form class="d-inline" action="{{route('document.destroy', $document)}}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-sm">
                                        @if($document->status == 'ACTIVO')
                                        <i class="fa fa-toggle-on text-success" aria-hidden="true"></i>
                                        @else
                                        <i class="fa fa-toggle-on text-danger" aria-hidden="true"></i>
                                        @endif
                                    </button>
                                </form>
                                {{-- <a href="{{route('user.destroy', $user)}}" class="btn btn-sm"><i class="fa fa-ban text-danger" aria-hidden="true"></i></a> --}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            {{$documents}} 
            </div>           
        </div>      
    @endsection