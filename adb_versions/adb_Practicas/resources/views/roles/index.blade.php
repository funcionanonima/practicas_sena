@extends('layouts.appBack')
    @section('content')
        {{-- bread --}}
        <nav aria-label="breadcrumb" class="my-4">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Roles</li>
            </ol>
        </nav>
        {{-- endbread --}}
        
        <h1 class="my-5 text-center">Roles Disponibles</h1>  

        <div class="card p-4">
            <table class="table table-hover table-striped border">
                <thead>
                    <tr>
                        <th >Nombre</th>
                        <th >Descripción</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($roles as $key=>$role)
                    <tr>
                        <td>{{$role->name}}</td>
                        <td>{{$role->body}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$roles}}            
        </div>     
         
    @endsection