@extends('layouts.appBack')

@section('content')
    {{-- bread --}}
    <nav aria-label="breadcrumb" class="my-4">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{route('report.index')}}">Reportes</a></li>
            <li class="breadcrumb-item active" aria-current="page">Editar Reporte {{$ac->name}}</li>
        </ol>
    </nav>
    {{-- endbread --}}
    <h1 class="my-5 text-center">Editar Reporte {{$ac->name}}</h1>   

    <div class="card p-5 m-2">

        <div class="mb-5">
            <a href="{{route('report.index')}}"><i class="fa fa-hand-o-left" aria-hidden="true"></i> Regresar</a>
        </div>

        <form method="POST" action="{{ route('report.update', $ac) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf

            <div class="row">
                
                <div class="form-group col-md-6">
                    <select id="companie_id" class="form-control @error('companie_id') is-invalid @enderror" name="companie_id" required >
                        <option value="{{$ac->companie->id}}">{{$ac->companie->name}}</option>
                        @foreach($companies as $companie)
                        <option value="{{$companie->id}}">{{$companie->name}}</option>
                        @endforeach
                    </select>                    
                    @error('companie_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>         

                <div class="form-group col-md-6">
                    <select id="area_id" class="form-control @error('area_id') is-invalid @enderror" name="area_id" required >
                        <option value="{{$ac->area->id}}">{{$ac->area->name}}</option>
                        @foreach($areas as $area)
                        <option value="{{$area->id}}">{{$area->name}}</option>
                        @endforeach
                    </select>                                                             
                    @error('area_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

            </div>  
            
            <div class="row">

                <div class="form-group col-md-12">
                    <textarea id="body" placeholder="Descripción" class="form-control @error('body') is-invalid @enderror" name="body" required autocomplete="new-body">{{$ac->body}}</textarea>
                    @error('body')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

            </div>    

            <div class="row border m-1">

                <div class="form-group col-12 d-inline">
                    <label for="image">Editar Portada: </label> 
                    <div class="form-group col-12"> 
                        <img class="img-fluid" src="/reports/{{$ac->image}}" alt="">
                    </div>
                    <input type="file" name="image" class="@error('image') is-invalid @enderror form-control-file" id="image">
                    @error('image')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

            </div>         

            <div class="row mt-5">
                <div class="form-group col text-center">
                    <button type="submit" class="btn btn-dark">
                        {{ __('Registrar') }}
                    </button>
                </div>                
            </div>
            
        </form> 

    </div>
@endsection
