@extends('layouts.appBack')
    @section('content')
        {{-- bread --}}
        <nav aria-label="breadcrumb" class="my-4">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
                @admin
                <li class="breadcrumb-item"><a href="{{route('report.index')}}">Reportes</a></li>
                @endadmin
                <li class="breadcrumb-item active" aria-current="page">Reporte {{$ac->area->name}} de {{$ac->companie->name}}</li>
            </ol>
        </nav>
        {{-- endbread --}}
        
        <h1 class="my-5 text-center">Reporte {{$ac->area->name}} de {{$ac->companie->name}}</h1> 
         
        <div class="card m-3 p-3">            
        
            <div class="container text-center"> 
                <img class="img-fluid" src="/reports/{{$ac->image}}" alt="">
            </div>
    
            <h2 class="my-5 d-block text-center">Comentarios</h2>   
            
            <div class="container-fluid mx-auto"> 
            <button type="button" class="mb-4 btn btn-dark" data-toggle="modal" data-target="#new">Nuevo</button>

            @include('partials.session_status')
               
            @admin          
            @if(count($ac->comments)>0)  
                @foreach($ac->comments->all() as $comment)                
                    <div class="alert alert-primary" role="alert">
                        <div class="col-12">
                            {{$comment->body}} 
                        </div>                                
                        <div class="col-12 text-right">
                            {{-- <a data-toggle="modal" data-target="#edit"><i class="fa fa-pencil text-warning" aria-hidden="true"></i></a> --}}
                            <form class="d-inline" action="{{route('comment.destroy', $comment)}}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-sm">
                                    @if($comment->status == 'ACTIVO')
                                    <i class="fa fa-toggle-on text-success" aria-hidden="true"></i>
                                    @else
                                    <i class="fa fa-toggle-on text-danger" aria-hidden="true"></i>
                                    @endif
                                </button>
                            </form>
                        </div>            
                    </div>                    
                @endforeach    
            @else
                <p class="text-center alert alert-warning"><i class="fa fa-puzzle-piece" aria-hidden="true"></i> Aún no se han creado recursos</p>           
            @endif          
            @endadmin   
            @user                                                      
            @if(count( $ac->comments()->where('status', 'ACTIVO')->get())>0)  
                @foreach($comments = $ac->comments()->where('status', 'ACTIVO')->get() as $comment)
                    <div class="alert alert-primary" role="alert">
                        <div class="col-12">
                            {{$comment->body}} 
                        </div>                                
                        <div class="col-12 text-right">
                            {{-- <a data-toggle="modal" data-target="#edit"><i class="fa fa-pencil text-warning" aria-hidden="true"></i></a> --}}
                            <form class="d-inline" action="{{route('comment.destroy', $comment)}}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-sm">
                                    @if($comment->status == 'ACTIVO')
                                    <i class="fa fa-toggle-on text-success" aria-hidden="true"></i>
                                    @else
                                    <i class="fa fa-toggle-on text-danger" aria-hidden="true"></i>
                                    @endif
                                </button>
                            </form>
                        </div>            
                    </div>                     
                @endforeach  
            @else
                <p class="text-center alert alert-warning"><i class="fa fa-puzzle-piece" aria-hidden="true"></i> Aún no se han creado recursos</p>           
            @endif            
            @enduser  

            </div>       

            {{-- modalNuevo --}}
            <div class="modal fade" id="new" tabindex="-1" role="dialog" aria-labelledby="new" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Nuevo Comentario</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body"> 
                            <form action="{{route('comment.store')}}" method="POST">                              
                                <div class="form-group">
                                    <textarea id="body" class="form-control @error('body') is-invalid @enderror" name="body" required placeholder="Descripción"></textarea>
                                    @error('body')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    <input type="number" name="area_companie_id" hidden value="{{$ac->id}}">
                                </div>                            
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    <button type="submit" class="btn btn-dark">Postear Comentario</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {{-- endmodal --}}          
    
            <h2 class="my-5 d-block text-center">Documentos</h2>
            
            <div class="container-fluid mx-auto">
                @isset($ac)
                    @if(count($ac->documents)>0)
                        <table class="table table-hover table-striped table-responsive-xl border">
                            <thead>
                                <tr>
                                    <th>Archivo</th>
                                    <th>Fecha</th>
                                    <th width="5%"class="text-center">Descargar</th> 
                                    @admin
                                    <th width="5%"class="text-center">Eliminar</th>    
                                    @endadmin                            
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($docs as $doc) 
                                <tr>
                                    <td><a href="/documents/{{$doc->path}}">{{$doc->name}} | {{$doc->id}}</a></td>
                                    <td>{{$doc->created_at}}</td>
                                    <td class="text-center">
                                        <a href="{{route('document.show', $doc)}}"><i class="fa fa-download" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                    @admin
                                    <td class="text-center">
                                        <form class="d-inline" action="" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-sm"><i class="fa fa-ban text-danger" aria-hidden="true"></i></button>
                                        </form>
                                        {{-- <a href="{{route('user.destroy', $user)}}" class="btn btn-sm"><i class="fa fa-ban text-danger" aria-hidden="true"></i></a> --}}
                                    </td>
                                    @endadmin
                                </tr>
                                @endforeach
                            </tbody>
                        </table> 
                    @else
                        <p class="text-center alert alert-warning"><i class="fa fa-puzzle-piece" aria-hidden="true"></i> Aún no se han creado recursos</p>
                    @endif
                @endisset      
            </div>     
            
            {{-- <h2 class="my-5 d-block text-center">Historial de Reportes</h2>
            
            <div class="container-fluid mx-auto">  
                <ul>
                        @foreach($reports->where('area_id', $ac->area->id)->where('companie_id', $ac->companie->id) as $report)
                        <li><a href="{{route('report.show', $report->id)}}">{{$report->id}} | {{$report->body}}</a>    area: {{$report->area_id}}|   empresa: {{$report->companie_id}}</li>
                        @endforeach

                </ul>
            </div>  --}}
        </div>
    @endsection