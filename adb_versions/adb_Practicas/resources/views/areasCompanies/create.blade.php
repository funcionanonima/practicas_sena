@extends('layouts.appBack')

@section('content')
    {{-- bread --}}
    <nav aria-label="breadcrumb" class="my-4">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{route('report.index')}}">Reportes</a></li>
            <li class="breadcrumb-item active" aria-current="page">Nuevo Reporte</li>
        </ol>
    </nav>
    {{-- endbread --}}
    <h1 class="my-5 text-center">Crear Reporte</h1> 
    
    <div class="card p-5 m-2">

        <div class="mb-5">
            <a href="{{route('report.index')}}"><i class="fa fa-hand-o-left" aria-hidden="true"></i> Regresar</a>
        </div>

        <form method="POST" action="{{ route('report.store') }}" enctype="multipart/form-data">
            @csrf

            <div class="row">

                <div class="form-group col-md-6">
                    <select id="companie_id" class="form-control @error('companie_id') is-invalid @enderror" name="companie_id" required >
                        <option selected value="">Empresa</option>
                        @foreach($companies as $companie)
                        <option value="{{$companie->id}}">{{$companie->name}}</option>
                        @endforeach
                    </select>
                    
                    @error('companie_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>              

                <div class="form-group col-md-6">
                    <select id="area_id" class="form-control @error('area_id') is-invalid @enderror" name="area_id" required >
                        <option selected value="">Área</option>
                        @foreach($areas as $area)
                        <option value="{{$area->id}}">{{$area->name}}</option>
                        @endforeach
                    </select>                                   
                                                    
                    @error('area_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>                               
                            
            <div class="row">
                <div class="form-group col-md-12">
                    <textarea id="body" placeholder="Descripción" class="form-control @error('body') is-invalid @enderror" name="body" required autocomplete="new-body"></textarea>
                    @error('body')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>     
            
            <div class="row border m-1">
                <div class="form-group col-12 d-inline">
                    <label for="image">Portada</label> 
                    <input type="file" name="image" class="form-control-file" id="image">
                </div>
                @error('image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>   
        
            <div class="row mt-5">
                <div class="form-group col text-center">
                    <button type="submit" class="btn btn-dark">
                        {{ __('Registrar') }}
                    </button>
                </div>                
            </div>

        </form>
                
    </div>
@endsection
