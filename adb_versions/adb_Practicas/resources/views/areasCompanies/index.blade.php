@extends('layouts.appBack')
    @section('content')
        {{-- bread --}}
        <nav aria-label="breadcrumb" class="my-4">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Reportes</li>
            </ol>
        </nav>
        {{-- endbread --}}
        <h1 class="my-5 text-center">Control Reportes</h1>         
         
        <div class="card p-4">

            <div class="mb-4 container-fluid">
                <a class="btn btn-dark" href="{{route('report.create')}}">Nuevo</a>
            </div>

            <div class="container-fluid mx-auto">
                <table class="table table-hover table-striped table-responsive-xl border">
                    <thead>
                        <tr>
                            <th>Descripción Breve</th>                         
                            <th>Empresa</th>
                            <th>Área</th>                      
                            <th>Fecha creación</th>
                            <th>Imagen</th>
                            <th width="5%" class="text-center">Editar</th> 
                            <th width="5%" class="text-center">Estado</th>                                
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($acs as $key=>$ac)
                        <tr>
                            <td><a href="{{route('report.show', $ac->id)}}">{{$ac->body}}</a></td>                        
                            <td>{{$ac->companie->name}}</td>
                            <td>{{$ac->area->name}}</td>    
                            <td>{{$ac->created_at}}</td>                    
                            <td><div class="my-auto" style="max-width:100px;"><a href="reports/{{$ac->image}}"><img class="img-fluid" src="reports/{{$ac->image}}"></a></div></td>                        
                            <td  width="5%" class="text-center">
                                <a href="{{route('report.edit', $ac->id)}}" class="btn btn-sm"><i class="fa fa-pencil text-warning" aria-hidden="true"></i></a>
                            </td>
                            <td  width="5%" class="text-center">
                                <form class="d-inline" action="{{route('report.destroy', $ac->id)}}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-sm"><i class="fa fa-ban text-danger" aria-hidden="true"></i></button>
                                </form>
                                {{-- <a href="{{route('user.destroy', $user)}}" class="btn btn-sm"><i class="fa fa-ban text-danger" aria-hidden="true"></i></a> --}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$acs}}     
            </div>       
        </div>      
    @endsection