@extends('layouts.appBack')

@section('content')
    {{-- bread --}}
    <nav aria-label="breadcrumb" class="my-4">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{route('idtype.index')}}">Tipos de Identificación</a></li>
            <li class="breadcrumb-item active" aria-current="page">Nuevo Tipo de Identificación</li>
        </ol>
    </nav>
    {{-- endbread --}}
    <h1 class="my-5 text-center">Registrar Tipos de Identificación</h1> 
    
    <div class="card p-5 m-2">

        <div class="mb-5">
            <a href="{{route('idtype.index')}}"><i class="fa fa-hand-o-left" aria-hidden="true"></i> Regresar</a>
        </div>

        <form method="POST" action="{{ route('idtype.store') }}">
            @csrf

            <div class="row">
                <div class="form-group col-md-6">
                    <input placeholder="Abreviación" id="abbreviation" type="abbreviation" class="form-control @error('abbreviation') is-invalid @enderror" name="abbreviation" value="{{ old('abbreviation') }}" required autocomplete="abbreviation">

                    @error('abbreviation')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    
                </div>

                <div class="form-group col-md-6">
                    <input placeholder="Nombre" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                </div>
            </div>                    

            <div class="row">
                <div class="form-group col-md-12">
                    <textarea placeholder="Descripción" id="body" class="form-control @error('body') is-invalid @enderror" name="body" required autocomplete="new-body"></textarea>
                    @error('body')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                </div>
            </div>        

            <div class="row mt-5">
                <div class="form-group col text-center">
                    <button type="submit" class="btn btn-dark">
                        {{ __('Registrar') }}
                    </button>
                </div>                
            </div>
            
        </form>

    </div>
@endsection
