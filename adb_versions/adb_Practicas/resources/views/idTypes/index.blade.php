@extends('layouts.appBack')
    @section('content')

        {{-- bread --}}
        <nav aria-label="breadcrumb" class="my-4">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Tipos de Identificación</li>
            </ol>
        </nav>
        {{-- endbread --}}        

        <h1 class="my-5 text-center">Control Tipos de Identificación</h1> 

        <div class="card p-4">

            <div class="mb-4 container-fluid">
                <a class="btn btn-dark" href="{{route('idtype.create')}}">Nuevo</a>
            </div>

            @include('partials.session_status')
         
            <div class="container-fluid mx-auto">
                <table class="table table-hover table-striped border">
                    <thead>
                        <tr>
                            <th >Abreviación</th>
                            <th >Nombre</th>
                            <th >Descripción</th>
                            <th width="5%"class="text-center">Editar</th> 
                            <th width="5%"class="text-center">Eliminar</th>                                
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($idTypes as $key=>$idType)
                        <tr>
                            <td>{{$idType->abbreviation}}</td>
                            <td>{{$idType->name}}</td>
                            <td>{{$idType->body}}</td>
                            <td class="text-center">
                                <a href="{{route('idtype.edit', $idType)}}" class="btn btn-sm"><i class="fa fa-pencil text-warning" aria-hidden="true"></i></a>
                            </td>
                            <td class="text-center">
                                <form class="d-inline" action="{{route('idtype.destroy', $idType)}}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-sm"><i class="fa fa-ban text-danger" aria-hidden="true"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$idTypes}}     
            </div>       
        </div>      
    @endsection