@extends('layouts.appBack')

@section('content')
    {{-- bread --}}
    <nav aria-label="breadcrumb" class="my-4">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{route('idtype.index')}}">Áreas</a></li>
            <li class="breadcrumb-item active" aria-current="page">Editar {{$area->name}}</li>
        </ol>
    </nav>
    {{-- endbread --}}
    <h1 class="my-5 text-center">Editar {{$area->name}}</h1> 
    
    <div class="card p-5 m-2">
            
        <div class="mb-5">
            <a href="{{route('area.index')}}"><i class="fa fa-hand-o-left" aria-hidden="true"></i> Regresar</a>
        </div>

        <form method="POST" action="{{ route('area.update', $area) }}">
            @method('PUT')
            @csrf

            <div class="row">
                    <div class="form-group col-md-12">
                        <input value="{{$area->name}}" placeholder="Nombre" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>                   
                </div>        

                <div class="row">
                    <div class="form-group col-md-12">
                        <textarea placeholder="Descipción" id="body" class="form-control @error('body') is-invalid @enderror" name="body" required autocomplete="new-body">{{$area->body}}</textarea>
                        @error('body')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="form-group col text-center">
                        <button type="submit" class="btn btn-dark">
                            {{ __('Editar') }}
                        </button>
                    </div>                
                </div>
        </form>

    </div>
@endsection
