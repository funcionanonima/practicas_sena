@extends('layouts.appBack')
    @section('content')
        {{-- bread --}}
        <nav aria-label="breadcrumb" class="my-4">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Áreas</li>
            </ol>
        </nav>
        {{-- endbread --}}
        <h1 class="my-5 text-center">Áreas</h1>
         
        <div class="card p-4">

            <div class="mb-4 container-fluid">
                <a class="btn btn-dark" href="{{route('area.create')}}">Nuevo</a>
            </div>

            @include('partials.session_status')

            <div class="container-fluid max-auto">
                <table class="table table-hover table-striped table-responsive-xl border">
                    <thead>
                        <tr>
                            <th>Nombre</th>                        
                            <th>Descripción</th>
                            <th width="5%"class="text-center">Editar</th> 
                            <th width="5%"class="text-center">Eliminar</th>                                
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($areas as $key=>$area)
                        <tr>
                            <td>{{$area->name}}</td>
                            <td>{{$area->body}}</td>
                            <td class="text-center">
                                <a href="{{route('area.edit', $area)}}" class="btn btn-sm"><i class="fa fa-pencil text-warning" aria-hidden="true"></i></a>
                            </td>
                            <td class="text-center">
                                <form class="d-inline" action="{{route('area.destroy', $area)}}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-sm"><i class="fa fa-ban text-danger" aria-hidden="true"></i></button>
                                </form>
                                {{-- <a href="{{route('user.destroy', $user)}}" class="btn btn-sm"><i class="fa fa-ban text-danger" aria-hidden="true"></i></a> --}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$areas}} 
            </div>
        </div>      
    @endsection