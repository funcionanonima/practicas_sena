@extends('layouts.appBack')
    @section('content')
        {{-- bread --}}
        <nav aria-label="breadcrumb" class="my-4">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Comentarios</li>
            </ol>
        </nav>
        {{-- endbread --}}
        <h1 class="my-5 text-center">Control Comentarios</h1>          
         
        <div class="card p-4">

            <div class="mb-4 container-fluid">
                <a class="btn btn-dark" href="{{route('comment.create')}}">Nuevo</a>
            </div>

            @include('partials.session_status')

            <div class="container-fluid mx-auto">
                <table class="table table-hover table-striped table-responsive-xl border">
                    <thead>
                        <tr>
                            <th>Reporte</th>
                            <th>Fecha Creación<th>
                            <th>Descripcion</th>
                            <th width="5%"class="text-center">Editar</th> 
                            <th width="5%"class="text-center">Estado</th>                                
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($comments as $key=>$comment)
                        <tr>
                            <td>{{$comment->report->companie->name}} | {{$comment->report->area->name}}</td>
                            <td>{{$comment->created_at}}<td>
                            <td>{{$comment->body}}</td>    
                            <td class="text-center">
                                <a href="{{route('comment.edit', $comment)}}" class="btn btn-sm"><i class="fa fa-pencil text-warning" aria-hidden="true"></i></a>
                            </td>
                            <td class="text-center">
                                <form class="d-inline" action="{{route('comment.destroy', $comment)}}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-sm">
                                        @if($comment->status == 'ACTIVO')
                                        <i class="fa fa-toggle-on text-success" aria-hidden="true"></i>
                                        @else
                                        <i class="fa fa-toggle-on text-danger" aria-hidden="true"></i>
                                        @endif
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$comments}}    
            </div>        
        </div>      
    @endsection