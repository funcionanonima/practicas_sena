@extends('layouts.appBack')

@section('content')
    {{-- bread --}}
    <nav aria-label="breadcrumb" class="my-4">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{route('comment.index')}}">Comentarios</a></li>
            <li class="breadcrumb-item active" aria-current="page">Nuevo comentario</li>
        </ol>
    </nav>
    {{-- endbread --}}
    <h1 class="my-5 text-center">Registrar Comentario</h1> 
    
    <div class="card m-2 p-5">

            <div class="mb-5">
                <a href="{{route('comment.index')}}"><i class="fa fa-hand-o-left" aria-hidden="true"></i> Regresar</a>
            </div>

            <form method="POST" action="{{ route('comment.store') }}">
                @csrf

                <div class="row">

                    <div class="form-group col-12">
                        
                        <select id="area_companie_id" class="form-control @error('area_companie_id') is-invalid @enderror" name="area_companie_id" required >
                                <option value="" selected disabled>Empresa y Área del Reporte</option>
                            @foreach($reports as $report)
                                <option value="{{$report->id}}">{{$report->companie->name.' '.$report->area->name.' | '.$report->body}}</option>
                            @endforeach
                        </select>
                        
                        @error('area_companie_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div> 

                <div class="row">

                    <div class="form-group col-12">
                        <textarea id="body" class="form-control @error('body') is-invalid @enderror" name="body" required autocomplete="new-body"></textarea>
                        @error('body')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div> 

                <div class="row mt-5">
                    <div class="form-group col text-center">
                        <button type="submit" class="btn btn-dark">
                            {{ __('Registrar') }}
                        </button>
                    </div>                
                </div>

            </form>

        </div>
        
@endsection
