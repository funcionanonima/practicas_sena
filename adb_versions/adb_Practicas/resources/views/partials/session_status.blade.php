@if(session('statusC'))
    <div class="alert alert-success alert-dismissible">
        {{session('statusC')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

@if(session('statusU'))
    <div class="alert alert-warning alert-dismissible">
        {{session('statusU')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

@if(session('statusD'))
    <div class="alert alert-danger alert-dismissible">
        {{session('statusD')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif