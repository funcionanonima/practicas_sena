@extends('layouts.appBack')

@section('content')
    {{-- bread --}}
    <nav aria-label="breadcrumb" class="my-4">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{route('user.index')}}">Usuarios</a></li>
            <li class="breadcrumb-item active" aria-current="page">Nuevo Usuario</li>
        </ol>
    </nav>
    {{-- endbread --}}
    <h1 class="my-5 text-center">Registrar Usuario</h1> 
    
    <div class="card p-5 m-2">

        <div class="mb-5">
            <a href="{{route('user.index')}}"><i class="fa fa-hand-o-left" aria-hidden="true"></i> Regresar</a>
        </div>

        {{-- @if($errors->any())
        {{dd($errors)}}
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        <ul>
        @endif --}}

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div class="row">

                <div class="form-group col-md-6">
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Nombre">
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Correo Electrónico">
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror                            
                </div>

            </div>

            <div class="row">

                <div class="form-group col-md-6">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="password" placeholder="Contraseña">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirme Contraseña">
                </div>

            </div>
                
            <div class="row">
                <div class="form-group col-md-6">
                    
                    <select id="id_type_id" class="form-control @error('id_type_id') is-invalid @enderror" name="id_type_id" required >
                        <option value="" selected disabled>Tipo de Identificación</option>
                        @foreach($idTypes as $idType)
                        <option value="{{$idType->id}}">{{$idType->name}}</option>
                        @endforeach
                    </select>                            
                    @error('id_type_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                </div>

                <div class="form-group col-md-6">
                        <input id="identification" type="string" class="form-control @error('identification') is-invalid @enderror" name="identification" required value="{{ old('identification') }}" placeholder="Documento">
                    @error('identification')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                </div>
            </div>

            <div class="row">

                <div class="form-group col-md-6">
                    <select id="role_id" class="form-control @error('role_id') is-invalid @enderror" name="role_id" required >
                        <option value="" selected disabled>Rol</option>
                        @foreach($roles as $role)
                        <option value="{{$role->id}}">{{$role->name}}</option>
                        @endforeach
                    </select>                                    
                    
                    @error('role_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                </div>

                <div class="form-group col-md-6">
                    <select id="status" class="form-control @error('status') is-invalid @enderror" name="status" required >
                        <option value="" selected disabled>Estado</option>
                        <option value="ACTIVO" >Activo</option>
                        <option value="INACTIVO" >Inactivo</option>
                    </select>                                    
                    
                    @error('status')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                </div>
            </div>

            <div class="row mt-5">
                <div class="form-group col text-center">
                    <button type="submit" class="btn btn-dark">
                        {{ __('Registrar') }}
                    </button>
                </div>                
            </div>
            
        </form>
        
    </div>
@endsection
