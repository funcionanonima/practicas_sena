@extends('layouts.app')
    @section('content')

    <div class="text-center my-5 p-5 bg-white">
        <div class="container">
            <h2 class="text-dark" >Contáctenos <i class="fa fa-envelope-open-o" aria-hidden="true"></i></h2> 
            <hr class="">
            <div class="row my-4">                
                <div class="col-sm-6">

                    @include('partials.session_status')
    
                    @if($errors->any())
                        @foreach($errors->all() as $error)
                            <div>{{$error}}</div>
                        @endforeach
                    @endif
    
                <form name="contact" class="text-center py-4" method="POST" action="{{route('contact.store')}}">
                        <p class="pt-3">Deje aquí su mensaje:</p>
    
                        <div class="row">
                            <div class="col-6 form-group">
                                <input type="text" class="form-control"  name="name" placeholder="Nombre">
                            </div>
                            <div class="col-6 form-group">
                                <input type="text" class="form-control"  name="companie" placeholder="Empresa">
                            </div>
                        </div>
    
                        <div class="row">
                            <div class="col form-group">
                                <input type="email" class="form-control"  name="email" placeholder="Email">
                            </div>
                        </div>
    
                        <div class="row">
                            <div class="col form-group">
                                <input class="form-control" type="tel" name="phone" pattern="^[0-9]{7,10}$" maxlenght="10" placeholder="Teléfono">
                            </div>
                        </div>
    
                        <div class="form-group">
                            <textarea class="form-control" name="body" id="" rows="3" placeholder="Mensaje"></textarea>
                        </div>
    
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="TerminosYCondiciones" name="TerminosYCondiciones" value="1">                        
                                <label class="custom-control-label" for="TerminosYCondiciones">
                                    <button style="font-size:x-small; line-height:1;" type="button" class="btn btn-link" data-toggle="modal" data-target="#ToS">
                                        Autorizo y acepto las polìticas de Habeas Data ADB Consulting
                                    </button>
                                </label>
                            </div>
                        </div>
    
                        <!-- Modal -->
                        <div class="modal fade" id="ToS" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Tèrminos y Condiciones</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                            Autorización para la recopilación y manejo de datos.- Autorizo la recolección, recopilación, gestión y manejo de mis datos, que voluntariamente revelo y como TITULAR de los datos aquí estipulados manifiesto que la información suministrada es cierta, no la he alterado ni he omitido ningún dato. Autorizo a ADB Consulting SAS a recopilar y usar esta información como registro de la realización de la actividad en la que participo.
    
                                            He sido advertido que ADB Consulting SAS publicó en ésta página: <a href="#">adbconsulting.co</a>  que tengo la opción para excluir mis datos personales de esta base de datos sino estoy de acuerdo con las políticas y procedimientos, haciendo dicha solicitud a la dirección de correo electrónico <a href="#">wilopez@adbconsulting.co</a>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
    
                        <div class="form-group mt-4">
                            <button class="btn btn-dark" onclick="pregunta()">Enviar</button>
                        </div>        
    
                    </form>
                    
                </div>
                <div class="col-sm-6 my-auto">
                    <p class="pt-3">CALLE 34 B # 65 D-02  OFICINA  201.<br> Edificio: Entre Calles, Medellín</p>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2804.5112680171114!2d-75.58678857659571!3d6.240273241028532!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e4429af8e9e8e25%3A0x7c5a6e02e4836a75!2sCalle%2034B%20%2365d-2%2C%20Medell%C3%ADn%2C%20Antioquia!5e0!3m2!1ses-419!2sco!4v1574176340308!5m2!1ses-419!2sco" width="300" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <hr>
        </div>
    </div>
    
    {{-- goupbutton --}}
    <button onclick="topFunction()" id="topBtn" title="Up" class="bg-warning text-white"><i class="fa fa-arrow-circle-up" aria-hidden="true"></i></button>
    </div>   
    <script language="JavaScript">
    function pregunta(){
        if (confirm('¿Estas seguro de enviar este formulario de contacto?')){
            document.tuformulario.submit()
        }
    }
    </script>     
@endsection