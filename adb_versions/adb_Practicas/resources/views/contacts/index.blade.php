@extends('layouts.appBack')
    @section('content')
        {{-- bread --}}
        <nav aria-label="breadcrumb" class="my-4">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Contactos</li>
            </ol>
        </nav>
        {{-- endbread --}}
        
        <h1 class="my-5 text-center">Mensajes de Contacto</h1>  

        <div class="card p-4">
            <table class="table table-hover table-striped border">
                <thead>
                    <tr>
                        <th >Nombre</th>
                        <th >Empresa</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($contacts as $key=>$contact)
                    <tr>
                        <td><a href="{{route('contact.show', $contact)}}">{{$contact->name}}</a></td>
                        <td>{{$contact->companie}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$contacts}}            
        </div>     
         
    @endsection