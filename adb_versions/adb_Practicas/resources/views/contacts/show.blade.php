@extends('layouts.appBack')
    @section('content')
        {{-- bread --}}
        <nav aria-label="breadcrumb" class="my-4">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
                @admin
                <li class="breadcrumb-item"><a href="{{route('contact.index')}}">Contáctos</a></li>
                @endadmin
                <li class="breadcrumb-item active" aria-current="page">Contácto {{$contact->name}}</li>
            </ol>
        </nav>
        {{-- endbread --}}
        
        <h1 class="my-5 text-center">Contácto {{$contact->name}}</h1> 
         
        <div class="card m-3 p-3">   
            
            <div class="py-4">
                <h3>Empresa</h3>
                {{$contact->companie}}
            </div>

            <div class="py-4">
                <h3>Correo Eléctronico</h3>
                {{$contact->email}}
            </div>

            <div class="py-4">
                <h3>Teléfono</h3>
                {{$contact->phone}}
            </div>

            <div class="py-4">
                <h3>Asunto</h3>
                {{$contact->body}}
            </div>
            
        </div>
    @endsection