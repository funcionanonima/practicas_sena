@extends('layouts.app')
    @section('content') 

    <div class="my-5 p-5 text-center bg-white"> 
        <div class="container">
            <h2 class="text-dark mb-4" >Nuestros Servicios </h2> 
            <hr class="">             
            <div class="row text-dark">
    
                <div class="col mx-4 text-center p-3">
                    <img src="{{ asset('images/LOGO_BLACK.jpg') }}" class="rot img-fluid rounded-circle border border-gray" alt="nada" style="max-width: 10rem">
                    <p class="mt-4">Implementamos modelos de gestión por procesos basados en ISO 9001 - EFQM.</p>
                </div>
    
                <div class="col text-center p-3">
                    <img src="{{ asset('images/LOGO_LIGHT.jpg') }}" class="rot img-fluid rounded-circle border border-gray" alt="nada" style="max-width: 10rem">
                    <p class="mt-4">Evaluación de la gestión de las entidades territoriales según el modelo MDM.</p>
                </div>
    
                <div class="col text-center p-3">
                    <img src="{{ asset('images/LOGO_BLACK.jpg') }}" class="rot img-fluid rounded-circle border border-gray" alt="nada" style="max-width: 10rem">
                    <p class="mt-4">Levantamiento, documentación y estandarización de procesos.</p>
                </div>
    
                <div class="col text-center p-3">
                    <img src="{{ asset('images/LOGO_LIGHT.jpg') }}" class="rot img-fluid rounded-circle border border-gray" alt="nada" style="max-width: 10rem">
                    <p class="mt-4">Procesos de formación en estrategía para entidades territoriales basados en el ecosistema de la gestión pública.</p>
                </div>
                            
                <div class="col text-center p-3">
                    <img src="{{ asset('images/LOGO_BLACK.jpg') }}" class="rot rounded-circle img-fluid border border-gray" alt="nada" style="max-width: 10rem">   
                    <p class="mt-4">Auditorias y certificación de procesos y sistmas tecnológicos.</p>
                </div>
    
                <div class="col text-center p-3">
                    <img src="{{ asset('images/LOGO_LIGHT.jpg') }}" class="rot img-fluid rounded-circle border border-gray" alt="nada" style="max-width: 10rem">
                    <p class="mt-4">Outsourcing para la gestión e implemenación de procesos administrativos y servicios.</p>
                </div>
    
            </div>
            <hr class=""> 
        </div>
    </div>

    <div class="text-center my-5 p-5 bg-white">    
        <div class="container">
            <h2 class="text-dark" >Nuestra visión</h2> 
            <hr class="">             
            <div class="row text-dark my-4">
                <div class="col text-center p-3">
                    <h2>Mision</h2>
                    <img src="{{ asset('images/LOGO_BLACK.jpg') }}" class="rotY my-5 img-fluid rounded-circle border border-gray" alt="nada" style="max-width: 10rem">
                    <p>Diseñar, mejorar y administrar procesos que permitan a nuestros aliados tomar decisiones estratégicas para generar valor a sus grupos de interés con responsabilidad, rentabilidad y crecimiento.  </p>
                </div>
                <div class="col text-center p-3">
                    <h2>Vision</h2>
                    <img src="{{ asset('images/LOGO_BLACK.jpg') }}" class="rotY my-5 img-fluid rounded-circle border border-gray" alt="nada" style="max-width: 10rem">  
                    <p>Ser el aliado estratégico de nuestros clientes reconocido por nuestra propuesta de valor y la generación de oportunidades de crecimiento.</p>
                </div>
            </div>
            <hr>
        </div>
    </div>

    <div class="text-center my-5 p-5 bg-white"> 
        <h2 class="text-dark" >Donde nos movemos?</h2> 
        {{-- <hr class="">    --}}
        <div class="row mt-5 text-dark">
            <div class="col">   
                <i class="fa fa-money fa-4x text-success" aria-hidden="true"></i>
                <p class="mt-3">Servicios Financieros</p>      
            </div>
            <div class="col">
                <i class="fa fa-desktop fa-4x text-secondary" aria-hidden="true"></i>
                <p class="mt-3">Servicios de Teconología</p>
            </div>
            <div class="col">
                <i class="fa fa-hand-peace-o fa-4x text-primary" aria-hidden="true"></i>
                <p class="mt-3">Servicios Sector solidario</p>
            </div>
            <div class="col">
                <i class="fa fa-globe fa-4x text-warning" aria-hidden="true"></i>
                <p class="mt-3">Entidades territoriales/Municipios</p>
            </div>
        </div>
        {{-- <hr> --}}
    </div>

    <div class="text-center my-5 p-5 bg-white">   
        <div class="container">
            <h2 class="text-dark" >Qué nos Apasiona?</h2> 
            <hr class="">  
            <div class="row mt-5 text-center">
                <div class="col-sm-6 col-md-3">
                    <div class="card mb-4 shadow-sm">
                        <img src="{{ asset('images/LOGO_BLACK.jpg') }}" class="img-fluid" alt="nada">                        
                        <div class="card-body">
                            <p class="card-text">Orientación al resultado</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="card mb-4 shadow-sm">
                        <img src="{{ asset('images/LOGO_LIGHT.jpg') }}" class="img-fluid" alt="nada">                        
                        <div class="card-body">
                            <p class="card-text">Generación de valor</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="card mb-4 shadow-sm">
                        <img src="{{ asset('images/LOGO_BLACK.jpg') }}" class="img-fluid" alt="nada">                        
                        <div class="card-body">
                            <p class="card-text">Estrategia</p>
                        </div>
                    </div>
                </div> 
                <div class="col-sm-6 col-md-3">
                    <div class="card mb-4 shadow-sm">
                        <img src="{{ asset('images/LOGO_LIGHT.jpg') }}" class="img-fluid" alt="nada">                   
                        <div class="card-body">
                            <p class="card-text">Generar una experiencia satisfactoria en nuestros clientes</p>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        </div>
    </div>

    {{-- goupbutton --}}
    <button onclick="topFunction()" id="topBtn" title="Up" class="bg-warning text-white"><i class="fa fa-arrow-circle-up" aria-hidden="true"></i></button>
    </div>        
    @endsection
