@extends('layouts.appBack')

@section('content')
    {{-- bread --}}
    <nav aria-label="breadcrumb" class="my-4">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{route('companie.index')}}">Empresas</a></li>
            <li class="breadcrumb-item active" aria-current="page">Editar Empresa</li>
        </ol>
    </nav>
    {{-- endbread --}}
    <h1 class="my-5 text-center">Editar Empresa {{$companie->name}}</h1> 
    
    <div class="p-5 m-2 card">

        <div class="mb-5">
            <a href="{{route('companie.index')}}"><i class="fa fa-hand-o-left" aria-hidden="true"></i> Regresar</a>
        </div>

        <div class="row justify-content-center">
            
            <div class="col">
                <form method="POST" action="{{ route('companie.update', $companie) }}">
                    @method('PUT')
                    @csrf

                    <div class="row">

                        <div class="form-group col-md-12">
                            <input id="name" value="{{$companie->name}}" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                        </div>
                    </div>
                        
                    <div class="row">
                        <div class="form-group col-md-6">
                            
                            <select id="id_type_id" class="form-control @error('id_type_id') is-invalid @enderror" name="id_type_id" required >
                                @foreach($idTypes as $idType)
                                    @if($idType->id == $companie->idType->id)
                                    <option value="{{$idType->id}}" selected>{{$idType->name}}</option>
                                    @else
                                    <option value="{{$idType->id}}">{{$idType->name}}</option>
                                    @endif
                                @endforeach
                            </select> 
                              
                            
                            @error('id_type_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                        </div>

                        <div class="form-group col-md-6">
                                <input id="identification" value="{{$companie->identification}} "type="string" class="form-control @error('identification') is-invalid @enderror" name="identification" required >

                            @error('identification')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                        </div>
                    </div>  

                    <div class="row">
                        <div class="form-group col-md-12">
                            <textarea id="body" class="form-control @error('body') is-invalid @enderror" name="body" required autocomplete="new-body">{{$companie->body}}</textarea>
                            @error('body')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                        </div>
                    </div> 

                    <div class="row mt-5">
                        <div class="form-group col text-center">
                            <button type="submit" class="btn btn-dark">
                                {{ __('Editar') }}
                            </button>
                        </div>                
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
@endsection
