@extends('layouts.appBack')

@section('content')
    {{-- bread --}}
    <nav aria-label="breadcrumb" class="my-4">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{route('companie.index')}}">Empresas</a></li>
            <li class="breadcrumb-item active" aria-current="page">Nueva Empresa</li>
        </ol>
    </nav>
    {{-- endbread --}}
    <h1 class="my-5 text-center">Registrar Empresa</h1> 
    
    <div class="card m-2 p-5">

        <div class="mb-5">
            <a href="{{route('companie.index')}}"><i class="fa fa-hand-o-left" aria-hidden="true"></i> Regresar</a>
        </div>

        <form method="POST" action="{{ route('companie.store') }}">
            @csrf

            <div class="row">
                <div class="form-group col-md-12">
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Nombre">
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                </div>
            </div>                        

            <div class="row">
                <div class="form-group col-md-4">                            
                    <select id="id_type_id" class="form-control @error('id_type_id') is-invalid @enderror" name="id_type_id" required >
                        <option value="" selected disabled>Tipo de Identificación</option>
                        @foreach($idTypes as $idType)
                        <option value="{{$idType->id}}">{{$idType->name}}</option>
                        @endforeach
                    </select>                           
                    @error('id_type_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group col-md-4">
                        <input id="identification" type="string" class="form-control @error('identification') is-invalid @enderror" name="identification" required placeholder="Identificación">
                    @error('identification')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group col-md-4">
                    <select id="status" class="form-control @error('status') is-invalid @enderror" name="status" required >
                        <option value="" selected disabled>Estado</option>
                        <option value="ACTIVO" >Activo</option>
                        <option value="INACTIVO" >Inactivo</option>
                    </select>                                    
                    
                    @error('status')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                </div>
            </div>  

            <div class="row">
                <div class="form-group col-md-12">
                    <textarea id="body" class="form-control @error('body') is-invalid @enderror" name="body" required autocomplete="new-body" placeholder="Breve Descripción"></textarea>
                    @error('body')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div> 

            <hr>

            <h3 class="text-center d-block my-4">Relacionar Usuarios:</h3>

            <div class="row">
                <div class="col-md-4 text-center m-auto">
                    <select name="users[]" multiple>
                        @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->name}}</option> 
                        @endforeach                           
                    </select>                    
                    @error('users')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror                    
                </div>
                <div class="col-md-8 m-auto" role="alert">
                    <p style="font-size:x-small;" class="alert alert-secondary">
                        Mantenga Presionado <span class="text-success">CTRL + click</span>  para seleccionar mas de 1 registro a la vez
                        <br>  
                        Mantenga presionado <span class="text-success">CTRL + click</span> para Quitar selección
                    </p>
                </div>
            </div>                     

            <div class="row mt-5">
                <div class="form-group col text-center">
                    <button type="submit" class="btn btn-dark">
                        {{ __('Registrar') }}
                    </button>
                </div>                
            </div>

        </form>
    </div>
@endsection
