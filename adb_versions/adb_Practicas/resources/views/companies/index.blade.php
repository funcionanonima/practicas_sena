@extends('layouts.appBack')
    @section('content')

        {{-- bread --}}
        <nav aria-label="breadcrumb" class="my-4">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Empresas</li>
            </ol>
        </nav>
        {{-- endbread --}}

        <h1 class="my-5 text-center">Control Empresas</h1> 

        <div class="card p-4">

                <div class="mb-4 container-fluid">
                    <a class="btn btn-dark" href="{{route('companie.create')}}">Nuevo</a>
                </div>

                @include('partials.session_status')
                     
                <div class="container-fluid mx-auto">
                    <table class="table table-hover table-striped table-responsive-xl border">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>TipoID</th>
                                <th>Identificacion</th>                          
                                <th>Breve Descripción</th>
                                <th width="5%"class="text-center">Editar</th> 
                                <th width="5%"class="text-center">Estado</th> 
                                {{-- <th width="5%"class="text-center">Eliminar</th>                                 --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($companies as $key=>$companie)
                            <tr>
                                <td>{{$companie->name}}</td>
                                <td>{{$companie->idType->abbreviation}}</td>
                                <td>{{$companie->identification}}</td>                        
                                <td>{{$companie->body}}</td>
                                <td class="text-center">
                                    <a href="{{route('companie.edit', $companie)}}" class="btn btn-sm"><i class="fa fa-pencil text-warning" aria-hidden="true"></i></a>
                                </td>
                                <td class="text-center">
                                    <form class="d-inline" action="{{route('companie.destroy', $companie)}}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-sm">
                                            @if($companie->status == 'ACTIVO')
                                            <i class="fa fa-toggle-on text-success" aria-hidden="true"></i>
                                            @else
                                            <i class="fa fa-toggle-on text-danger" aria-hidden="true"></i>
                                            @endif
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{$companies}}            
                </div>     
        </div>     
    @endsection