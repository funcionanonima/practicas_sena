@extends('layouts.appBack')
    @section('content')
        {{-- bread --}}
        <nav aria-label="breadcrumb" class="my-4">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Usuarios</li>
            </ol>
        </nav>
        {{-- endbread --}}
        <h1 class="my-5 text-center">Control Usuarios</h1>  

        <div class="card p-4">               

            <div class="mb-4 container-fluid">
                <a class="btn btn-dark" href="{{route('register')}}">Nuevo</a>
            </div>        

            @include('partials.session_status')

            <div class="container-fluid mx-auto">
                <table class="table table-hover table-striped table-responsive-xl border">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>TipoID</th>
                            <th>Identificacion</th>                                                
                            <th>Emaill</th>
                            <th>Rol</th>
                            <th width="5%"class="text-center">Editar</th> 
                            <th width="5%"class="text-center">Estado</th>                                
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $key=>$user)
                        <tr>
                            <td>{{$user->name}}</td>
                            <td>{{$user->idType->name}}</td>
                            <td>{{$user->identification}}</td>                        
                            <td>{{$user->email}}</td>
                            <td>{{$user->role->name}}</td>
                            <td class="text-center">
                                <a href="{{route('user.edit', $user)}}" class="btn btn-sm"><i class="fa fa-pencil text-warning" aria-hidden="true"></i></a>
                            </td>
                            <td class="text-center">
                                <form class="d-inline" action="{{route('user.destroy', $user)}}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-sm">
                                        @if($user->status == 'ACTIVO')
                                        <i class="fa fa-toggle-on text-success" aria-hidden="true"></i>
                                        @else
                                        <i class="fa fa-toggle-on text-danger" aria-hidden="true"></i>
                                        @endif
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$users}}            
            </div>      
       </div>
    @endsection