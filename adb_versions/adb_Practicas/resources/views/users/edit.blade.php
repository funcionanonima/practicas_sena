@extends('layouts.appBack')

@section('content')
    {{-- bread --}}

    <nav aria-label="breadcrumb" class="my-4">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{route('user.index')}}">Usuarios</a></li>
            <li class="breadcrumb-item active" aria-current="page">Editar Usuario {{$user->name}}</li>
        </ol>
    </nav>
    {{-- endbread --}}

    <h1 class="my-5 text-center">Editar Usuario {{$user->name}}</h1>    

    <div class="card p-5 m-2">

        <div class="mb-5">
            <a href="{{route('user.index')}}"><i class="fa fa-hand-o-left" aria-hidden="true"></i> Regresar</a>
        </div>
        @if($errors->any())
            @foreach($errors->all() as $error)
                <div>{{$error}}</div>
            @endforeach
        @endif
        <form method="POST" class="" action="{{ route('user.update', $user) }}">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="form-group col-md-6">
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$user->name}}" required placeholder="Nombre">
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$user->email}}" required autocomplete="email" placeholder="Correo Electrónico">
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror                            
                </div>
            </div>           
            <div class="row">
                <div class="form-group col-md-4">                    
                    <select id="id_type_id" class="form-control @error('id_type_id') is-invalid @enderror" name="id_type_id" required >
                        @foreach($idTypes as $idType)
                            @if($idType->id == $user->idType->id)
                            <option value="{{$idType->id}}" selected>{{$idType->name}}</option>
                            @else
                            <option value="{{$idType->id}}">{{$idType->name}}</option>
                            @endif
                        @endforeach
                    </select>                            
                    @error('id_type_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group col-md-4">
                        <input id="identification" type="string" class="form-control @error('identification') is-invalid @enderror" name="identification" required placeholder="Documento"value="{{$user->identification}}">
                    @error('identification')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group col-md-4">
                    <select id="role_id" class="form-control @error('role_id') is-invalid @enderror" name="role_id" required >
                        @foreach($roles as $role)
                            @if($role->id == $user->role->id)
                            <option value="{{$role->id}}" selected>{{$role->name}}</option>
                            @else
                            <option value="{{$role->id}}" selected>{{$role->name}}</option>
                            @endif
                        @endforeach
                    </select>                                  
                    @error('role_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            
            <div class="row mt-5">
                <div class="form-group col text-center">
                    <button type="submit" class="btn btn-dark">
                        {{ __('Editar') }}
                    </button>
                </div>                
            </div>

        </form>

    </div>
@endsection