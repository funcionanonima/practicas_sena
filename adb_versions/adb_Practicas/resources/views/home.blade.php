@extends('layouts.appBack')

    @section('content')
        {{-- bread --}}
        <nav aria-label="breadcrumb" class="my-4">
            <ol class="breadcrumb">
                <li class="breadcrumb-itemactive" aria-current="page">Inicio</li>
            </ol>
        </nav>
        {{-- endbread --}}
        @user

        {{-- <div class="container my-5"> --}}
            <div class="accordion p-2" id="accordionExample">
                @foreach($companies as $key=>$companie) 
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{$key}}" aria-expanded="true" aria-controls="collapseOne">
                                {{$companie->name}}
                            </button>
                        </h2>
                    </div>
                
                    <div id="collapse{{$key}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body p-5">
                                {{$companie->body}}
                        </div>
                        <div class="container-fluid mx-auto">                            
                            @if(count($companie->areas)>0)  
                                @foreach($areas = $companie->areas()->distinct()->get() as $area)                                 
                                    <a href="{{route('report.show', $report = $companie->areascompanies->where('area_id', $area->id)->last())}}"
                                        class="mb-2 list-group-item list-group-item-action">
                                    <i class="fa fa-search" aria-hidden="true"></i> {{$area->name}}</a>                                
                                @endforeach 
                            @else
                                <ul>
                                    <i class="fa fa-exclamation-triangle text-secondary" aria-hidden="true"></i> No tiene áreas asociadas
                                </ul>                                
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        {{-- <div> --}}
            
        @enduser

    @endsection
