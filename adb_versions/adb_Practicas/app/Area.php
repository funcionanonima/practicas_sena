<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    public function companies(){
        return $this->belongsToMany(Companie::class, 'areas_companies');
    }  

    public function areacompanies(){
        return $this->hasMany(AreaCompanie::class);
    }  
    
    protected $table = 'areas';

    protected $fillable = [
        'name', 'body', 
    ];
}
