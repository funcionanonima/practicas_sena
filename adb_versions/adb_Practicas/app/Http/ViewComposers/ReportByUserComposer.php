<?php 

namespace App\Http\ViewComposers;
 
use Auth;
use Illuminate\Support\Collec;
use App\Http\ViewComposers\stdClass;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use App\AreaCompanie;
 
class ReportByUserComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view){
        // $comps = auth()->user()->companies;
        // foreach($objectA as $k => $v) $objectB->$k = $v;
        // $compsList = new \stdClass();
        // foreach($comps as $key => $comp){            
        //     collect($compsList->$key = $comp);
        // }
        // foreach($comps as $key => $comp){
        //     $areas[$key] = $comp->areas;
            // foreach($areas as $key2 => $area){
            //     $areas[][$key2] = $area[$key2]->areacompanies; 
            // }
        // }
        // dd($compsList);
        // $view->with(); 
        $reps = DB::table('areas_companies')
            ->join('companies', 'companies.id', '=',  'areas_companies.companie_id')
            ->join('companie_user', 'companie_user.companie_id', '=', 'companies.id')
            ->where('companie_user.user_id', '=', auth()->user()->id)
            ->select('areas_companies.id', 'areas_companies.companie_id', 'areas_companies.area_id')
            ->get();
        // dd($reps);
    } 
}