<?php 

namespace App\Http\ViewComposers;
 
use Auth;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use App\Area;
 
class AreasIdNameComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view){
            $view->with('areas', $areas = Area::select('id', 'name')->get()); 
            // dd($areas);
    }
 
}