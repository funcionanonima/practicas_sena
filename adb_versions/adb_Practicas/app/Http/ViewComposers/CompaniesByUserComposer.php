<?php 

namespace App\Http\ViewComposers;
 
use Auth;
use Illuminate\Support\Collection;
use Illuminate\Contracts\View\View;
use App\Companie;
 
class CompaniesByUserComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view){
        if((auth()->user()->role()->first()->name) != 'admin'){
            $companies = auth()->user()->companies()->where('status', 'ACTIVO')->distinct()->get();
            // dd($companies);
            $view->with('companies', $companies);   
        }
    }

    // public function compose(View $view)
    // {        
    //     $view->with('comps', $comps = 
    //         DB::table('companies')
    //         ->join('data_companies', 'companies.id', '=', 'data_companies.companie_id')
    //         ->join('data_companies', 'areas.id', '=', 'data_companies.area.id')
    //         ->select('companies.name', 'companies.id')
    //         ->where('data_companies.companie_id', '=', auth()->user()->id)
    //         // ->distinct()
    //         ->get()
    //     );
    //     dd($comps);
    // }
 
}