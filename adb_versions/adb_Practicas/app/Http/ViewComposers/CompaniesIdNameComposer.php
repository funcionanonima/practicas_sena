<?php 

namespace App\Http\ViewComposers;
 
use Illuminate\Support\Collection;
use Illuminate\Contracts\View\View;
use App\Companie;
 
class CompaniesIdNameComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view){
        $view->with('companies', $companies = Companie::select('id', 'name')->where('status', 'ACTIVO')->get());   
        // dd($companies);
    }
 
}