<?php 

namespace App\Http\ViewComposers;
 
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use App\Role;
 
class RolesIdNameComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view){
        $roles = Role::all();       
        $view->with('roles',  $roles);
        
    }
 
}