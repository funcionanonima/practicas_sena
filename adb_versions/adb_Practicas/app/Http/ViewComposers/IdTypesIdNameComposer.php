<?php 

namespace App\Http\ViewComposers;
 
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use App\IdType;
 
class IdTypesIdNameComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view){
        $idTypes = IdType::all();       
        $view->with('idTypes',  $idTypes);
        
    }
 
}