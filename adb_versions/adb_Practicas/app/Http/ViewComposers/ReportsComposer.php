<?php 

namespace App\Http\ViewComposers;
 
use Illuminate\Contracts\View\View;
use App\AreaCompanie;
 
class ReportsComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view){
            $view->with('reports', $reports = AreaCompanie::orderBy('created_at', 'desc' )->select('id', 'area_id', 'companie_id', 'body')->get()); 
            // dd($reports);
    }
 
}