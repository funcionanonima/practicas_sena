<?php 

namespace App\Http\ViewComposers;
 
use Illuminate\Support\Collection;
use Illuminate\Contracts\View\View;
use App\User;
 
class UsersIdComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view){
        $view->with('users', $users = User::select('id', 'name')->where('status', 'ACTIVO')->get());   
        // dd($users);
    }
 
}