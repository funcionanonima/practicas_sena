<?php

namespace App\Http\Controllers;

use App\Document;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $documents = Document::orderBy('created_at', 'desc')->paginate(10);
        return view('documents.index', compact('documents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('documents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|string|max:45',
            'file' => 'required|file',
            'area_companie_id' => 'required|integer',
            'status' => 'required|string',
        ]);
        // dd($request->file('path'));
        if($request->hasFile('path')){
            $file = $request->file('path');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/documents/', $name);
        }
        $document = new Document();
        $document->area_companie_id = $request->area_companie_id;
        $document->name = $request->name;
        $document->path = $name;
        $document->status = $request->status;
        $document->save();

        return redirect()->route('document.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        $path = 'documents/'.$document->path;
        return response()->download($path);
        // return response()->download($path);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function edit(Document $document)
    {
        return view('documents.edit', compact('document'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $document)
    {
        if($request->hasFile('path')){
            $file = $request->file('path');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/documents/', $name);
        }
        $document->area_companie_id = $request->area_companie_id;
        $document->name = $request->name;
        $document->path = $name;
        $document->save();

        return redirect()->route('document.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function destroy(Document $document)
    {
        if($document->status == 'ACTIVO'){
            $document->status = 'INACTIVO';
        }elseif($document->status == 'INACTIVO'){
            $document->status = 'ACTIVO';
        }        
        $document->save();
        return redirect()->route('document.index');
    }
}
