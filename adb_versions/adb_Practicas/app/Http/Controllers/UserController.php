<?php

namespace App\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    
    public function index(){
        $users = User::orderBy('created_at', 'desc')->paginate(10);
        return view('users.index', ['users'=>$users]);
    }

    public function create(User $user){
        return view('auth.register');
    }

    public function show(User $user)
    {
        return $user;
    }

    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }
    
    public function update(Request $request, User $user){
        $validateData = $request->validate([
            'name' => 'required|string|max:45',
            'email' => ['required',
            Rule::unique('users')->ignore($user->id)],
            'identification' => ['required',
            Rule::unique('users')->ignore($user->id)],
            'id_type_id' => 'required|integer',
            'role_id' => 'required|integer',
        ]);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->identification = $request->identification;
        $user->role_id = $request->role_id;
        $user->id_type_id = $request->id_type_id;
        $user->save();

        return redirect()->route('user.index')->with('statusU', 'Usuario '.$user->name.' Editado');
    }

    public function destroy(User $user){
        if($user->status == 'ACTIVO'){
            $user->status = 'INACTIVO';
        }elseif($user->status == 'INACTIVO'){
            $user->status = 'ACTIVO';
        }        
        $user->save();
        return redirect()->route('user.index')->with('statusU', 'El Estado de '.$user->name.' cambiado a: '.$user->status);
    }
}
