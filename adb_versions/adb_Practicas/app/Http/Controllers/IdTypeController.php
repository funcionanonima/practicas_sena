<?php

namespace App\Http\Controllers;

use App\IdType;
use Illuminate\Http\Request;

class IdTypeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idTypes = Idtype::orderBy('created_at', 'desc')->paginate(10);
        return view('idTypes.index', compact('idTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('idTypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|string|max:45|unique:id_types',
            'abbreviation' => 'required|string|max:10|unique:id_types',
            'body' => 'required|string|max:200'
        ]);
        $idtype = new IdType();
        $idtype->abbreviation = $request->abbreviation;
        $idtype->name = $request->name;
        $idtype->body = $request->body;
        $idtype->save();

        return redirect()->route('idtype.index')->with('statusC', 'Tipo de Identificación '.$idtype->name.' Creada!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IdType  $idType
     * @return \Illuminate\Http\Response
     */
    public function show(IdType $idType)
    {
        return $idType;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IdType  $idType
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $idType = IdType::where('id',$id)->first();
        return view('idTypes.edit', compact('idType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IdType  $idType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'name' => ['required',
            Rule::unique('id_types')->ignore($id)],
            'abbreviation' => ['required',
            Rule::unique('id_types')->ignore($id)],
            'body' => 'required|string|max:200'
        ]);
        $idType = IdType::where('id',$id)->first();
        $idType->abbreviation = $request->abbreviation;
        $idType->name = $request->name;
        $idType->body = $request->body;
        $idType->save();

        return redirect()->route('idtype.index')->with('statusU', 'Tipo de Identificación '.$request->name.' Editada!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IdType  $idType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        IdType::findOrFail($id)->delete();
        return redirect()->route('idtype.index')->with('statusD', 'Tipo de Identificación Eliminada!');
    }
}
