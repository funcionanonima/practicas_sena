<?php

namespace App\Http\Controllers;

use App\AreaCompanie;
use Illuminate\Http\Request;

class AreaCompanieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $acs = AreaCompanie::orderBy('created_at', 'desc')->paginate(10);
        return view('areasCompanies.index', compact('acs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('areasCompanies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {             
        $ac = new AreaCompanie();
        $ac->area_id = $request->area_id;
        $ac->companie_id = $request->companie_id;
        if($request->hasFile('image')){
            $file = $request->file('image');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/reports/', $name);
            $ac->image = $name;
        }          
        $ac->body = $request->body;
        $ac->save();

        return redirect()->route('report.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ac = AreaCompanie::findOrFail($id);
        $docs = $ac->documents()->orderBy('created_at', 'desc')->where('status', 'ACTIVO')->get();
        // dd($docs);
        return view('areasCompanies.show', compact('ac', 'docs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ac = AreaCompanie::findOrFail($id);
        return view('areasCompanies.edit', compact('ac'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ac = AreaCompanie::findOrFail($id);
        $ac->area_id = $request->area_id;
        $ac->companie_id = $request->companie_id;  
        if($request->hasFile('image')){
            $file = $request->file('image');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/reports/', $name);
            $ac->image = $name;
        }               
        $ac->body = $request->body;
        $ac->save();

        return redirect()->route('report.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ac = AreaCompanie::findOrFail($id);
        $ac->delete(); 
        return redirect()->route('report.index');
    }
}
