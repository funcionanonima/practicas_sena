<?php

namespace App\Http\Controllers;

use App\Companie;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CompanieController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Companie::orderBy('created_at', 'desc')->paginate(10);
        return view('companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|string|max:45',
            'identification' => 'required|string|max:55|unique:companies',
            'id_type_id' => 'required|integer',
            'status' => 'required|string',
            'body' => 'required|string|max:200'
        ]);
        $comp = new Companie();
        $comp->identification = $request->identification;
        $comp->id_type_id = $request->id_type_id;
        $comp->name = $request->name;
        $comp->body = $request->body;
        $comp->status = $request->status;
        $comp->save();
        
        $users = $request->users;
        // dd($users);
        if($users){
            foreach($users as $user){
                $comp->users()->attach($user);
            }
        }       

        return redirect()->route('companie.index')->with('statusC', 'Empresa '.$comp->name.' creada correctamente');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Companie  $companie
     * @return \Illuminate\Http\Response
     */
    public function show(Companie $companie)
    {
        // return $companie;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Companie  $companie
     * @return \Illuminate\Http\Response
     */
    public function edit(Companie $companie)
    {
        return view('companies.edit', compact('companie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Companie  $companie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Companie $companie)
    {
        $validateData = $request->validate([
            'name' => 'required|string|max:45',
            'identification' => ['required',
            Rule::unique('companies')->ignore($companie->id)],
            'id_type_id' => 'required|integer',
            'body' => 'required|string|max:200'
        ]);
        $companie->name = $request->name;
        $companie->identification = $request->identification;
        $companie->id_type_id = $request->id_type_id;
        $companie->body = $request->body;
        $companie->save();

        return redirect()->route('companie.index')->with('statusU', 'La empresa '.$companie->name.' a sido editada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Companie  $companie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Companie $companie)
    {
        if($companie->status == 'ACTIVO'){
            $companie->status = 'INACTIVO';
        }elseif($companie->status == 'INACTIVO'){
            $companie->status = 'ACTIVO';
        }        
        $companie->save();
        return redirect()->route('companie.index')->with('statusU', 'El Estado de '.$companie->name.' cambiado a: '.$companie->status);
    }
    
}
