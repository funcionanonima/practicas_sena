<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{     
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::orderBy('created_at', 'desc')->paginate(10);
        return view('comments.index', compact('comments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('comments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'body' => 'required|string||max:200',
            'area_companie_id' => 'required|string',
        ]);
        $comm = new Comment();
        $comm->area_companie_id = $request->area_companie_id;
        $comm->body = $request->body;
        $comm->status = 'ACTIVO';
        $comm->save();

        return redirect()->back()->with('statusC', 'Comentario Creado');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        // return $comment;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        return view('comments.edit', compact('comment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        $validateData = $request->validate([
            'body' => 'required|string||max:200',
            'area_companie_id' => 'required|string',
        ]);
        $comment->area_companie_id = $request->area_companie_id;
        $comment->body = $request->body;
        $comment->save();

        return redirect()->back()->with('statusU', 'Comentario Editado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        if($comment->status == 'ACTIVO'){
            $comment->status = 'INACTIVO';
        }elseif($comment->status == 'INACTIVO'){
            $comment->status = 'ACTIVO';
        }        
        $comment->save();

        return redirect()->back()->with('statusU', 'Estado del comentario Cambiado');
    }
}
