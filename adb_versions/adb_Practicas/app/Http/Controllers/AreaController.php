<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Area;
use Illuminate\Http\Request;

class AreaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $areas = Area::orderBy('created_at', 'desc')->paginate(10);
        return view('areas.index', compact('areas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('areas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|string|max:45',
            'body' => 'required|string|max:200'
        ]);
        $area = new Area();
        $area->name = $request->name;
        $area->body = $request->body;
        $area->save();

        return redirect()->route('area.index')->with('statusC', 'Area '.$area->name.' Creada!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function show(Area $area)
    {
        return $area;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function edit(Area $area)
    {
        return view('areas.edit', compact('area'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Area $area)
    {
        $validateData = $request->validate([
            'name' => 'required|string|max:45',
            'body' => 'required|string|max:200'
        ]);
        $area->name = $request->name;
        $area->body = $request->body;
        $area->save();

        return redirect()->route('area.index')->with('statusU', 'Area '.$area->name.' Editada!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function destroy(Area $area)
    {
        $area->delete();
        return redirect()->route('area.index')->with('statusD', 'Area '.$area->name.' Eliminada!');
    }
    
}
