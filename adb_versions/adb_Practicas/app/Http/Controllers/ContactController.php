<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts =  Contact::orderBy('created_at', 'desc')->paginate(10);
        return view('contacts.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:45',
            'email' => 'required|string|email|max:55',
            'companie' => 'required|string|min:8',
            'phone' => 'required|integer|min:7',
            'body' => 'required|string',
            'TerminosYCondiciones' => 'required|boolean'
        ]);
        // return dd($request);
        if($request->TerminosYCondiciones === "1"){
            $contact = new Contact();
            $contact->name = $request->name;
            $contact->companie = $request->companie;
            $contact->email = $request->email;
            $contact->phone = $request->phone;
            $contact->body = $request->body;
            $contact->TC = $request->TerminosYCondiciones;        

            $contact->save();
            return redirect()->route('mailus')->with('statusC', 'Mensaje enviado con éxito');
        }
    return "error";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        // $contact = Contact::get($contact);
        // dd($contact);
        return view('contacts.show', compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        //
    }
}
