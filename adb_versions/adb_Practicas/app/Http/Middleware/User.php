<?php

namespace App\Http\Middleware;

use Closure;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $auth = auth()->user();
        if($auth != null){
            if((auth()->user()->role()->first()->name) == 'usuario'){
                return $next($request);
            }else{
                return redirect('login');
            } 
        }else{
            return redirect('login');
        } 
    }
}
