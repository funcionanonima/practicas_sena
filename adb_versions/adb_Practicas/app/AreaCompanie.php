<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreaCompanie extends Model
{
    public function area(){
        return $this->belongsTo(Area::class);
    }

    public function companie(){
        return $this->belongsTo(Companie::class);
    }

    public function documents(){
        return $this->hasMany(Document::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class)->orderBy('created_at', 'desc');
    }

    protected $table = 'areas_companies';

    protected $fillable = [
        'area_id', 'companie_id', 'body', 'image',
    ];
}
