<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function report(){
        return $this->belongsTo(AreaCompanie::class, 'area_companie_id');
    }
    
    protected $table = 'comments';

    protected $fillable = [
        'body', 'area_companie_id', 'status'
    ];
}
