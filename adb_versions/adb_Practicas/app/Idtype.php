<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IdType extends Model
{
    public function users(){
        return $this->hasMany(User::class);
    }

    public function companies(){
        return $this->hasMany(User::class);
    }

    protected $table = 'id_types';

    protected $fillable = [
        'name', 'body', 'abbreviation',
    ];
}
