<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companie extends Model
{
    public function idType(){
        return $this->belongsTo(Idtype::class);
    }
    
    public function users(){
        return $this->belongsToMany(User::class);
    }

    public function areas(){
        return $this->belongsToMany(Area::class, 'areas_companies');
    }

    public function areascompanies(){
        return $this->hasMany(AreaCompanie::class);
    }
    
    protected $table = 'companies';

    protected $fillable = [
        'name', 'identification', 'body', 'id_type_id', "status",
    ];
}
