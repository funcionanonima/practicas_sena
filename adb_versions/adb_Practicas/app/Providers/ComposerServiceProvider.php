<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composers([
            'App\Http\ViewComposers\CompaniesByUserComposer'=>
                [
                    'layouts.appBack',
                    'home',
                ],
            'App\Http\ViewComposers\IdTypesIdNameComposer'=>
                [
                    'auth.register',
                    'users.edit',
                    'companies.edit',
                    'companies.create',
                ],
            'App\Http\ViewComposers\RolesIdNameComposer'=>
                [
                    'auth.register',
                    'users.edit',
                ],
             'App\Http\ViewComposers\CompaniesIdNameComposer'=>
                [
                    'areasCompanies.create',
                    'areasCompanies.edit',
                ],
            'App\Http\ViewComposers\AreasIdNameComposer'=>
                [
                    'areasCompanies.create',
                    'areasCompanies.edit',
                ],
            'App\Http\ViewComposers\ReportByUserComposer'=>
                [
                    'home',
                ],
            'App\Http\ViewComposers\ReportsComposer'=>
                [
                    'comments.create',
                    'comments.edit',
                    'documents.create',
                    'documents.edit',
                    'areasCompanies.show',
                ],
            'App\Http\ViewComposers\UsersIdComposer'=>
                [
                    'companies.create',
                ],
            ]);
    }
}
