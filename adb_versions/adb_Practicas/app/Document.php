<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    public function report(){
        return $this->belongsTo(AreaCompanie::class, 'area_companie_id');
    }
    
    protected $table = 'documents';

    protected $fillable = [
        'path', 'status', 'name', 'area_companie_id',
    ];
}
